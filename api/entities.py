from app import db
from constants.security_constants import SecurityConstants


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100))
    password = db.Column(db.String(100))
    name = db.Column(db.String(100))

    def __init__(self, email, password, name, phone):
        self.email = email
        self.password = password
        self.name = name
        self.phone = phone
        self.rol = SecurityConstants.ROL_ADMIN

    def __str__(self):
        return self.email
