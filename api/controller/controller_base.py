from app import app
from decorators.api_base import api
from flask import jsonify


@app.route('/api/health', methods=['GET'])
@api.exception
def index():
    return jsonify({"status": "OK"}), 200
