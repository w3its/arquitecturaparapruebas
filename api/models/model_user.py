class UserModel():

    def toJson(user):
        return {"id": user.id, "email": user.email, "rol": user.rol, "name": user.name}

    def toJsonArray(items):
        list = []
        for item in items:
            list.append(UserModel.toJson(item))
        return list

    def jsonSchema():
        schema = {
            "type": "object",
            "properties": {
                "email": {"type": "string"},
                "password": {"type": "string"},
            },
            "required": ["email", "password"]
        }
        return schema
