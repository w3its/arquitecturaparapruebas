# README

## Proyecto Base Python

El proyecto utiliza flask, sqlalchemy como ORM, para las migraciones se utiliza alembic via flask-migrate, psycopg2 como conector para postgres y jsonschema para las validaciones.

### Entidades

Las entidades se crean en **entities.py**

Una vez creada una entidad es importante hacerlo en la base de datos mediante migrations, de la misma forma al realizar cambios en la misma deberan luego ejecutarse el migrate.

#### Migraciones

Al iniciar el proceso de migraciones (esto se realiza solo la primera vez para crear todo el sistema de migraciones en la base) debe correrse el init

```bash
docker exec -it flask_base_app_1 python3 manage.py db init
```

Luego el comando para crear las migraciones para nuevas entidades o cambios en las entidades es la siguiente

```bash
docker exec -it flask_base_app_1 python3 manage.py db migrate
```

Luego para ejecutar los cambios de las migraciones en la base hay que correr el upgrade, importante que al bajarse otra rama o cambios debe siempre ejecutarse el upgrade para dejar la base en orden.

```bash
docker exec -it flask_base_app_1 python3 manage.py db upgrade
```

### Controllers

Al crear un nuevo controller es importante agregarlo al `app.py`

### Modelo / View model

Para retornar un model debe crearse en la carpeta de models

Contiene la serializacion del objeto a json, la serializacion de una coleccion del objeto y el esquema para cargar las validaciones

#### Validaciones

Para las validaciones se usa la libreria de jsonschema que permite cargar una estructura para realizar la validacion del objeto