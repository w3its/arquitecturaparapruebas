from app import db
from entities import User


class RepositoryUser():

    def create(self, email: str, name: str) -> User:
        user = User(email, "INITIAL_PASSWORD_TO_CHANGE", name)
        db.session.add(user)
        db.session.commit()
        db.session.refresh(user)
        return user

    def find_user_by_email(self, email):
        user = User.query.filter_by(email=email).one_or_none()
        return user

    def get_user_by_id(self, id):
        user = User.query.filter_by(id=id).one_or_none()
        return user
