import unittest


class TestControllerLogin(unittest.TestCase):

    def test_unsuccessful_login_due_to_non_existent_user(self):
        pass

    def test_login_succesfull(self):
        pass

    def test_unsuccessful_login_due_to_invalid_password(self):
        pass

    def test_unsuccessful_login_due_to_more_than_one_user_with_the_same_email(self):
        pass

    def test_invalid_request_login_without_parameters(self):
        pass


if __name__ == '__main__':
    unittest.main()
